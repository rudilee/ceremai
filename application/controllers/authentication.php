<?php

class Authentication_Controller extends Base_Controller {

	public $restful = TRUE;

	public function get_index() {
		$result = array();
		$status = 200;

		$result['hash'] = Hash::make('pass');

		return Response::json($result, $status);
	}

	public function get_login() {
		$result = array();
		$status = 200;

		if (Auth::check()) {
			$result['message'] = 'Anda sudah login';
		} else {
			$credentials = array(
				'username' => Input::get('username'),
				'password' => Input::get('password')
			);

			if (!Auth::attempt($credentials)) {
				$status = 403;
				$result['message'] = 'Username atau password anda salah!';
			} else {
				$user = Auth::user();
				$role = User::role();
				
				$result['user_id'] = $user->id;
				$result['username'] = $user->username;
				
				$last_name = !empty($user->last_name) ? ' ' . $user->last_name : '';
				$result['fullname'] = $user->first_name . $last_name;
				
				$result['role'] = $role->name;
				$result['role_name'] = ucwords($role->name);
				$result['role_id'] = $role->id;

				$result['datetime'] = date('Y-m-d H:i:s');
			}
		}

		return Response::json($result, $status);
	}

	public function get_logout() {
		Auth::logout();

		return Response::json(array('message' => 'Anda telah berhasil logout'), 200);
	}
	
	public function get_user() {
		$user = Auth::user();
		$last_name = !empty($user->last_name) ? ' ' . $user->last_name : '';
		
		$result = array(
			'user_id' => $user->id,
			'username' => $user->username,
			'fullname' => $user->first_name . $last_name
		);
		
		return Response::json($result, 200);
	}
}