<?php

class Call_Activity_Controller extends Base_Controller {

	public $restful = TRUE;

	public function get_status() {

		return Response::json(DB::table('call_status')->get());
	}

	public function get_reason() {
		return Response::json(DB::table('call_reason')->get());
	}
	
	public function get_called() {
		return Response::json(array('called' => date('Y-m-d H:i:s')));
	}
	
	public function post_activity($customer_id = 0) {
		$result = array();
		$status = 200;

		if (!empty($customer_id)) {
			$input = Input::all();
			
			$rules = array(
				'called' => 'required|max:20',
				'called_number' => 'required|max:50',
				'follow_up' => 'max:50',
				'call_status_id' => 'integer',
				'call_reason_id' => 'integer',
				'remarks' => 'max:255'
			);
			
			$validation = Validator::make($input, $rules);
			
			if ($validation->fails()) {
				$status = 400;
				
				$result['message'] = json_encode($validation->errors->messages);
			} else {
				$values = array(
					'user_id' => Auth::user()->id,
					'called' => $input['called'],
					'called_number' => $input['called_number'],
					'call_status_id' => $input['call_status_id'],
					'call_reason_id' => $input['call_reason_id']
				);
				
				if (Input::has('follow_up')) {
					$values['follow_up'] = $input['follow_up'];
				} else {
					$values['follow_up'] = null;
				}
				
				if (Input::has('remarks')) {
					$values['remarks'] = $input['remarks'];
				} else {
					$values['remarks'] = null;
				}
				
				$call_activity = DB::table('call_activity');
				$call_activity->where('customer_id', '=', $customer_id);
				
				$success = false;
				$count = $call_activity->count();
				if (!empty($count)) {
					$affected = $call_activity->update($values);
					$success = !empty($affected);
				} else {
					$values['customer_id'] = $customer_id;
					
					$success = $call_activity->insert($values);
				}
				
				if ($success) {
					$result['message'] = 'Call activity telah berhasil ditambahkan';
				}
			}
		}

		return Response::json($result, $status);
	}
	
	public function get_history($customer_id) {
		$result = array();
		$status = 200;

		$query = DB::table('call_activity_all')->where('customer_id', '=', $customer_id);
		$query->left_join('users', 'call_activity_all.user_id', '=', 'users.id');
		$query->left_join('call_status', 'call_activity_all.call_status_id', '=', 'call_status.id');
		$query->left_join('call_reason', 'call_activity_all.call_reason_id', '=', 'call_reason.id');
		$query->order_by('called', 'DESC');
		
		$result = $query->get(array(
			'called', 'called_number', 'follow_up', 'call_status.status AS call_status', 'call_reason.reason AS call_reason', 
			'remarks', 'users.username', 'users.first_name', 'users.last_name'
		));

		return Response::json($result, $status);
	}
}