<?php

class Call_Scoring_Controller extends Base_Controller {

	public $restful = TRUE;

	public function get_criteria() {
		return Response::json(DB::table('call_scoring_criteria')->get());
	}

	public function get_group() {
		return Response::json(DB::table('call_scoring_criteria_group')->get());
	}
	
	public function get_check($customer_id = 0) {
		$result = array();
		$status = 200;

		if (!empty($customer_id)) {
			$result = DB::table('call_scoring')->where('customer_id', '=', $customer_id)->get('id');
			$result = is_array($result) ? $result[0] : $result;
		}

		return Response::json($result, $status);
	}
	
	public function put_score($customer_id = 0) {
		$result = array();
		$status = 200;

		if (!empty($customer_id)) {
			$call_scoring_id = DB::table('call_scoring')->insert_get_id(array(
				'customer_id' => $customer_id,
				'user_id' => Auth::user()->id,
				'product' => Input::get('product'),
				'selling_date' => Input::get('selling_date'),
				'duration' => Input::get('duration')
			));
			
			$goods = Input::get('good');
			$averages = Input::get('average');
			$poors = Input::get('poor');
			
			if (!empty($call_scoring_id)) {
				$values = array('scoring_id' => $call_scoring_id);
				
				foreach ($goods as $key => $good) {
					$values['criteria_id'] = $key;
					$values['score'] = $good == 1 ? 'Good' : ($averages[$key] == 1 ? 'Average' : ($poors[$key] == 1 ? 'Poor' : DB::raw('NULL')));
					
					DB::table('call_scoring_score')->insert($values);
					
					$result = array('message' => 'Call score telah berhasil di simpan');
				}
			}
		}

		return Response::json($result, $status);
	}

}