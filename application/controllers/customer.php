<?php

class Customer_Controller extends Base_Controller {

	public $restful = TRUE;
	
	public function get_index() {
		return $this->get_page(1);
	}

	public function get_detail($id = 0) {
		$result = array();
		$status = 200;

		if(!empty($id)) {
			$customer = DB::table('customers');
			$customer->left_join('phones_additional', 'customers.id', '=', 'phones_additional.customer_id');
			$customer->left_join('address_additional', 'customers.id', '=', 'address_additional.customer_id');
			$customer->where('customers.id', '=', $id);
			$customer->left_join('assignments', 'customers.id', '=', 'assignments.customer_id');
			
			if (User::role()->name != 'quality assurance') {
				$customer->where_in('assignments.user_id', Children::all());
			}
			
			$customer->left_join('campaigns', 'assignments.campaign_id', '=', 'campaigns.id');
			
			$result = $customer->take(1)->get(array(
				'customers.id', 'code', 'campaigns.name as campaign', 'customers.name', 'sex', 'date_of_birth', 'employer',
				'home_street_1', 'home_street_2', 'home_street_3', 'home_city', 'home_postal_code',
				'office_street_1', 'office_street_2', 'office_street_3', 'office_city', 'office_postal_code',
				'street_1 AS additional_street_1', 'street_2 AS additional_street_2', 'street_3 AS additional_street_3', 
				'city AS additional_city', 'postal_code AS additional_postal_code',
				'home_phone', 'home_phone_2', 'home_phone_3', 'office_phone', 'office_phone_2', 'office_phone_3',
				'mobile_phone', 'mobile_phone_2', 'mobile_phone_3',
				DB::raw("DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(date_of_birth)),'%Y')+0 AS age"),
				'memo'
			));
			
			$result = !empty($result) ? $result[0] : $result;
		}

		return Response::json($result, $status);
	}

	public function get_page($page = 0) {
		$result = array();
		$status = 200;

		$per_page = 50;

		Input::replace(array('page' => $page));

		$customers = DB::table('customers');
		$customers->left_join('phones_additional', 'customers.id', '=', 'phones_additional.customer_id');
		$customers->left_join('address_additional', 'customers.id', '=', 'address_additional.customer_id');
		$customers->left_join('assignments', 'customers.id', '=', 'assignments.customer_id');
		$customers->where_in('assignments.user_id', Children::all());
		$customers->left_join('campaigns', 'assignments.campaign_id', '=', 'campaigns.id');
		$customers->left_join('users', 'assignments.user_id', '=', 'users.id');
		$customers->left_join('call_activity', 'customers.id', '=', 'call_activity.customer_id');
		$customers->left_join('call_reason', 'call_activity.call_reason_id', '=', 'call_reason.id');

		if (Input::has('name')) {
			$firstname = Input::get('name');

			$customers->where('customers.name', 'LIKE', "%$firstname%");
		}

		if (Input::has('sex')) {
			$customers->where('sex', '=', Input::get('sex'));
		}

		if (Input::has('dob')) {
			$customers->where('date_of_birth', '=', Input::get('dob'));
		}

		if (Input::has('address')) {
			$customers->where(function($query) {
				$address = Input::get('address');

				$query->where('home_street_1', 'LIKE', "%$address%")
					  ->or_where('home_street_2', 'LIKE', "%$address%")
					  ->or_where('home_street_3', 'LIKE', "%$address%")
					  ->or_where('office_street_1', 'LIKE', "%$address%")
					  ->or_where('office_street_2', 'LIKE', "%$address%")
					  ->or_where('office_street_3', 'LIKE', "%$address%")
					  ->or_where('street_1', 'LIKE', "%$address%")
					  ->or_where('street_2', 'LIKE', "%$address%")
					  ->or_where('street_3', 'LIKE', "%$address%");
			});
		}

		if (Input::has('city')) {
			$city = Input::get('city');

			$customers->where('home_city', 'LIKE', "%$city%")
					  ->or_where('office_city', 'LIKE', "%$city%")
					  ->or_where('city', 'LIKE', "%$city%");
		}

		if (Input::has('postal_code')) {
			$postal_code = Input::get('postal_code');

			$customers->where('home_postal_code', 'LIKE', "%$postal_code%")
					  ->or_where('office_postal_code', 'LIKE', "%$postal_code%")
					  ->or_where('postal_code', 'LIKE', "%$postal_code%");
		}

		if (Input::has('phone')) {
			$customers->where(function($query) {
				$phone = Input::get('phone');

				$query->where('home_phone', 'LIKE', "%$phone%")
					  ->or_where('home_phone_2', 'LIKE', "%$phone%")
					  ->or_where('home_phone_3', 'LIKE', "%$phone%")
					  ->or_where('office_phone', 'LIKE', "%$phone%")
					  ->or_where('office_phone_2', 'LIKE', "%$phone%")
					  ->or_where('office_phone_3', 'LIKE', "%$phone%")
					  ->or_where('mobile_phone', 'LIKE', "%$phone%")
					  ->or_where('mobile_phone_2', 'LIKE', "%$phone%")
					  ->or_where('mobile_phone_3', 'LIKE', "%$phone%");
			});
		}
		
		if (Input::has('code')) {
			$code = Input::get('code');
			
			$customers->where('code', 'LIKE', "%$code%");
		}
		
		if (Input::has('campaign_id')) {
			$customers->where('campaigns.id', '=', Input::get('campaign_id'));
		}

		if (Input::has('call_reason_id')) {
			$customers->where('call_activity.call_reason_id', '=', Input::get('call_reason_id'));
		}
		
		if (Input::has('call_date_from') && Input::has('call_date_to')) {
			$called_from = Input::get('call_date_from');
			$called_to = Input::get('call_date_to');
			
			$customers->where('called', 'BETWEEN', DB::raw("'$called_from 00:00:00' AND '$called_to 23:59:59'"));
		}
		
		if (Input::has('polis_status')) {
			$customers->where('product_lpp_polis.status', '=', Input::get('polis_status'));
		}
		
		$paginator = $customers->paginate($per_page, array(
			'customers.id', 'code', 'campaigns.name as campaign', 'customers.name', 'date_of_birth', 'sex',
			DB::raw("CONCAT_WS(' - ', username, CONCAT_WS(' ', first_name, last_name)) AS agent"),
			'call_reason.reason AS call_reason', 'called AS call_date', 'call_activity.remarks',
			'customers.home_phone', 'customers.office_phone', 'customers.mobile_phone'
		));

		if ($page > 0) {
			$result = $paginator->results;
		} else {
			$result = array('last' => $paginator->last, 'total' => $paginator->total);
		}

		return Response::json($result, $status);
	}
	
	public function put_phone($customer_id = 0) {
		$result = array();
		$status = 200;

		if(!empty($customer_id)) {
			$input = Input::all();
			$phones = DB::table('phones_additional')->where('customer_id', '=', $customer_id);
			$values = array('user_id' => Auth::user()->id);
			
			$phone_types = array('home', 'office', 'mobile');
			foreach ($phone_types as $phone_type) {
				for ($i = 2; $i <= 3; $i++) {
					$phone_field = $phone_type . '_phone_' . $i;
					$values[$phone_field] = $input[$phone_field];
				}
			}
			
			if (!empty($values)) {
				$count = $phones->count();
				if (!empty($count)) {
					$phones->update($values);
				} else {
					$values['customer_id'] = $customer_id;
					
					$phones->insert($values);
				}
			}
			
			$result['message'] = 'Update nomor telepon additional berhasil';
		}

		return Response::json($result, $status);
	}
	
	public function put_address($customer_id = 0) {
		$result = array();
		$status = 200;

		if(!empty($customer_id)) {
			$input = Input::all();
			$address = DB::table('address_additional')->where('customer_id', '=', $customer_id);
			$values = array('user_id' => Auth::user()->id);
			
			if (Input::has('street_1')) {
				$values['street_1'] = Input::get('street_1');
			}
			
			if (Input::has('street_2')) {
				$values['street_2'] = Input::get('street_2');
			}
			
			if (Input::has('street_3')) {
				$values['street_3'] = Input::get('street_3');
			}
			
			if (Input::has('city')) {
				$values['city'] = Input::get('city');
			}
			
			if (Input::has('postal_code')) {
				$values['postal_code'] = Input::get('postal_code');
			}
			
			if (!empty($values)) {
				$count = $address->count();
				if (!empty($count)) {
					$address->update($values);
				} else {
					$values['customer_id'] = $customer_id;
					
					$address->insert($values);
				}
			}
			
			$result['message'] = 'Update alamat additional berhasil';
		}

		return Response::json($result, $status);
	}

}