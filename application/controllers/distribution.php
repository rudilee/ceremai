<?php

class Distribution_Controller extends Base_Controller {

	public $restful = TRUE;
	
	public function get_quota($campaign_id = 0) {
		$result = array();
		$status = 200;

		if (!empty($campaign_id)) {
			$resultRaw = DB::query('SELECT count(*) AS quota FROM assignments WHERE campaign_id = ? AND user_id = ?', 
						 array($campaign_id, Auth::user()->id));
			
			$result = $resultRaw[0];
		}

		return Response::json($result, $status);
	}
	
	public function get_user($campaign_id = 0) {
		$result = array();
		$status = 200;

		$users = DB::table('users');
		$users->where_in('users.id', Children::all());
		$users->left_join('users_role', 'users.id', '=', 'users_role.user_id');
		$users->left_join('role', 'users_role.role_id', '=', 'role.id');
		
		$and_campaign = '';
		
		if (!empty($campaign_id) && is_numeric($campaign_id)) {
			$and_campaign = "AND assignments.campaign_id = $campaign_id";
		}
		
		$result = $users->get(array(
			'users.id', 'username', 'first_name', 'last_name', 'role_id',
			DB::raw("(SELECT count(*) FROM assignments WHERE assignments.user_id = users.id $and_campaign GROUP BY assignments.user_id) AS quota"),
			DB::raw("(SELECT count(*) FROM assignments, call_activity WHERE assignments.customer_id = call_activity.customer_id AND assignments.user_id = users.id $and_campaign GROUP BY assignments.user_id) AS utilised")
		));

		return Response::json($result, $status);
	}
	
	public function put_transfer() {
		$result = array();
		$status = 200;

		if (Input::has('campaign_id') && Input::has('allocation') && Input::has('user_ids')) {
			$auth_user_id = Auth::user()->id;
			$campaign_id = Input::get('campaign_id');
			$allocation = Input::get('allocation');
			$user_ids = Input::get('user_ids');
			
			if (is_array($user_ids)) {
				foreach ($user_ids as $user_id) {
					DB::query(
						'UPDATE assignments SET user_id = ? WHERE user_id = ? AND campaign_id = ? LIMIT ?',
						array(intval($user_id), $auth_user_id, $campaign_id, intval($allocation))
					);
				}
			}
			
			$result['message'] = 'Transfer data berhasil dilakukan';
		}

		return Response::json($result, $status);
	}
	
	public function put_withdraw() {
		$result = array();
		$status = 200;

		if (Input::has('campaign_id') && Input::has('user_ids')) {
			$affected = 0;
			$auth_user_id = Auth::user()->id;
			$campaign_id = Input::get('campaign_id');
			$user_ids = Input::get('user_ids');
			
			if (is_array($user_ids)) {
				foreach ($user_ids as $user_id) {
					$affected += DB::query(
						'UPDATE assignments ass LEFT JOIN call_activity ca ON ass.customer_id = ca.customer_id SET ass.user_id = ? ' . 
						'WHERE ass.user_id = ? AND ass.campaign_id = ? AND ca.customer_id IS NULL',
						array($auth_user_id, $user_id, $campaign_id)
					);
				}
			}
			
			$result['message'] = "Withdraw data sejumlah $affected berhasil dilakukan";
		}

		return Response::json($result, $status);
	}

}