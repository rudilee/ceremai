<?php

class Export_Controller extends Base_Controller {

	public $restful = TRUE;
	
	public function __call($name, $arguments) {
		$response = array('result' => array(), 'status' => 200, 'type' => 'application/json');
		
		if (!Auth::check()) {
			$response['status'] = 401;
			$response['result'] = array('message' => 'Anda belum login');
		} else {
			$result = call_user_func_array(array($this, $name), $arguments);
			if (is_array($result)) {
				if (isset($result[0])) {
					$response['result'] = $result[0];
				}
				
				if (isset($result[1])) {
					$response['status'] = $result[1];
				}
				
				if (isset($result[2])) {
					$response['type'] = $result[2];
				}
			}
		}
		
		$response_result = $response['result'];
		
		if ($response['type'] == 'application/json') {
			$response_result = json_encode($response['result']);
		}
		
		return Response::make($response_result, $response['status'], array('Content-Type' => $response['type']));
	}
	
	private function get_excel($name, $product) {
		if (Auth::user()->role == 'agent') return;
		
		include 'PHPExcel.php';

		$excel = new PHPExcel();
		$worksheet = $excel->getActiveSheet();

		$headers = array();
		$column_range = '';
		
		$table = null;
		
		switch ($name) {
			case 'ok_file':
				switch ($product) {
					case 'mv':
						$headers = array();
						
						$column_range = '';
						
						break;
					case 'lpp':
						$headers = array(
							'no', 'recordnr', 'cif', 'tarp', 'email', 'rel1', 'rel2', 'rel3', 'cardtype', 'cardprefix', 'agent', 'callresult',
							'callstatus', 'rejectrsn', 'card_number_completed_diff_requested_prefix', 'addr1', 'addr2', 'addr3', 'city', 'zip',
							'mobile', 'office', 'office2', 'home', 'fax', 'plan', 'premi', 'benef1', 'benef2', 'benef3', 'salesdate', 'title',
							'total_member', 'dob_1', 'name_1', 'sex_1', 'rel_1', 'kind_of_disease_1', 'dob_2', 'name_2', 'sex_2', 'rel_2',
							'expired_date', 'agent_name', 'spv_name', 'atm_name'
						);
						
						$column_range = 'A1:AT1';
						
						break;
				}
				
				$table = $this->get_ok_file($product);
				
				break;
			case 'recurring':
				switch ($product) {
					case 'mv':
						$headers = array(
							'INSURED', 'INCEPTION', 'EXPIRY', 'REFNO', 'NO_OF_RISK' , 'CONJ_POLICYNO', 'CONJ_CERTIFICATENO', 
							'OBJ_INFO_01', 'OBJ_INFO_02', 'OBJ_INFO_03' , 'OBJ_INFO_04', 'OBJ_INFO_05', 'OBJ_INFO_06', 
							'OBJ_INFO_07', 'OBJ_INFO_08' , 'OBJ_INFO_09', 'OBJ_INFO_10', 'OBJ_INFO_11', 'OBJ_INFO_12', 
							'OBJ_INFO_13' , 'OBJ_INFO_14', 'OBJ_INFO_15', 'RISKLOCATIONREMARK', 'MAINPCTADJ', 'ADDINTERESTCODE1' , 
							'ADDINTERESTREMARK1', 'ADDCURRENCY1', 'ADDSUMINSURED1', 'ADDPCTADJ1' , 'ADDINTERESTCODE2', 
							'ADDINTERESTREMARK2', 'ADDCURRENCY2', 'ADDSUMINSURED2' , 'ADDPCTADJ2', 'ADDINTERESTCODE3', 
							'ADDINTERESTREMARK3', 'ADDCURRENCY3' , 'ADDSUMINSURED3', 'ADDPCTADJ3', 'ADDINTERESTCODE4', 
							'ADDINTERESTREMARK4' , 'ADDCURRENCY4', 'ADDSUMINSURED4', 'ADDPCTADJ4', 'ADDINTERESTCODE5' , 
							'ADDINTERESTREMARK5', 'ADDCURRENCY5', 'ADDSUMINSURED5', 'ADDPCTADJ5', 'PRODUCT' , 'TOPRO_CURRENCY', 
							'TOPRO_MAINSUMINSURED', 'DISCOUNT', 'INSTALLMENT_FREQ' , 'INSTALLMENT_PERIOD', 'INSTALLMENT_FIRSTDUE'
						);
						
						$column_range = 'A1:BD1';
						
						break;
					case 'lpp':
						$headers = array(
							'Nomor_Register', 'Nomor_Referensi', 'Nomor_Kartu_Kredit', 'Masa_Berlaku_Kredit', 'Jumlah_Transaksi', 
							'Decimal_Point', 'Nama_Pemilik_Kartu', 'Description', 'Filler', 'Alamat_Pemilik_Kartu', 'Nomor_Telephone', 
							'Periode', 'agent_name', 'spv_name', 'atm_name'
						);
						
						$column_range = 'A1:O1';
						
						break;
				}
				
				$table = $this->get_recurring($product);
				
				break;
		}
		
		$worksheet->fromArray($headers);
		
		$style = array(
			'font' => array(
				'bold' => true
			),
		);
		
		$worksheet->getStyle($column_range)->applyFromArray($style);
		
		$table_array = array();
		
		//$row = 1;
		foreach($table[0] as $table_object) {
			$table_array[] = get_object_vars($table_object);
			
			/*$column = 1;
			foreach (get_object_vars($table_object) as $field) {
				$worksheet->setCellValueByColumnAndRow($column, $row, $field, true)->setDataType(PHPExcel_Cell_DataType::TYPE_STRING);
				
				$column++;
			}
			
			$row++;*/
		}
		
		$worksheet->fromArray($table_array, NULL, 'A2');
		
		$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		
		ob_start();
		$writer->save('php://output');
		$result = ob_get_contents();
		ob_end_clean();
		
		return array($result, 200, 'application/vnd.ms-excel');
	}
	
	private function get_ok_file($product) {
		if (Auth::user()->role == 'agent') return;
		
		if (empty($product)) return;
		
		$ok_file = null;
		
		switch($product) {
			case 'mv':
				$ok_file = DB::table('product_mv_polis');
				$ok_file->left_join('product_mv_vehicles', 'product_mv_polis.id', '=', 'polis_id');
				
				break;
			case 'lpp':
				$ok_file = DB::table('product_lpp_polis');
				//$ok_file->where('status', '=', 'Approved');
				$ok_file->left_join('product_lpp_plan', 'product_lpp_polis.plan_id', '=', 'product_lpp_plan.id');
				$ok_file->left_join(
					'product_lpp_premi',
					DB::raw("DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(IF(insured_type_1 = 'Main Insured', date_of_birth_1, date_of_birth_2))),'%Y')+0"),
					'BETWEEN',
					DB::raw('age_min and age_max and product_lpp_polis.plan_id = product_lpp_premi.plan_id')
				);
				$ok_file->left_join('users_role_level', 'product_lpp_polis.user_id', '=', 'users_role_level.user_id');
				$ok_file->left_join('customers', 'product_lpp_polis.customer_id', '=', 'customers.id');
				$ok_file->left_join('address_additional', 'product_lpp_polis.customer_id', '=', 'address_additional.customer_id');
				
				break;
		}
		
		if (empty($ok_file)) return;
		
		if (Input::has('created_from') && Input::has('created_to')) {
			$created_from = Input::get('created_from');
			$created_to = Input::get('created_to');
			
			$ok_file->where('updated', 'BETWEEN', DB::raw("'$created_from 00:00:00' AND '$created_to 23:59:59'"));
		}
		
		if (Input::has('plan_id')) {
			$ok_file->where('plan_id', '=', Input::get('plan_id'));
		}
		
		if (Input::has('status')) {
			$ok_file->where('status', '=', Input::get('status'));
		}
		
		$fields = array();
		
		switch($product) {
			case 'mv':
				$fields = array();
				
				break;
			case 'lpp':
				$fields = array(
					'product_lpp_polis.id AS no',
					'customers.code AS recordnr',
					DB::raw('NULL AS cif'),
					DB::raw('NULL AS tarp'),
					DB::raw('NULL AS email'),
					'beneficiary_relation_1 AS rel1',
					'beneficiary_relation_2 AS rel2',
					'beneficiary_relation_3 AS rel3',
					DB::raw("CONCAT_WS(' ', issuing_bank, card_type) AS cardtype"),
					'card_prefix AS cardprefix',
					'agent',
					DB::raw('NULL AS callresult'),
					DB::raw('NULL AS callstatus'),
					DB::raw('NULL AS rejectrsn'),
					DB::raw("CONCAT_WS(' ', SUBSTR(card_number, 1, 4), SUBSTR(card_number, 5, 4), SUBSTR(card_number, 9, 4), SUBSTR(card_number, 13, 4)) AS card_number_completed_diff_requested_prefix"),
					DB::raw("IF(address = 'Home Address', home_street_1, IF(address = 'Office Address', office_street_1, street_1)) AS addr1"),
					DB::raw("IF(address = 'Home Address', home_street_2, IF(address = 'Office Address', office_street_2, street_2)) AS addr2"),
					DB::raw("IF(address = 'Home Address', home_street_3, IF(address = 'Office Address', office_street_3, street_3)) AS addr3"),
					DB::raw("IF(address = 'Home Address', home_city, IF(address = 'Office Address', office_city, city)) AS city"),
					DB::raw("IF(address = 'Home Address', home_postal_code, IF(address = 'Office Address', office_postal_code, postal_code)) AS zip"), 
					DB::raw("CONCAT(product_lpp_polis.mobile_phone, ' ') AS mobile"),
					DB::raw("CONCAT(product_lpp_polis.office_phone, ' ') AS office"),
					DB::raw('NULL AS office2'),
					DB::raw("CONCAT(product_lpp_polis.home_phone, ' ') AS home"),
					DB::raw('NULL AS fax'),
					'product_lpp_plan.name AS plan',
					'monthly_amount AS premi',
					'beneficiary_name_1 AS benef1',
					'beneficiary_name_2 AS benef2',
					'beneficiary_name_3 AS benef3',
					'created AS salesdate',
					DB::raw("IF(sex = 'MALE', 'BAPAK', 'IBU') AS title"),
					DB::raw('1 AS total_member'),
					'date_of_birth_1 AS dob_1',
					'name_1',
					'sex_1',
					'insured_type_1 AS rel_1',
					'kind_of_disease_1',
					'date_of_birth_2 AS dob_2',
					'name_2',
					'sex_2',
					'insured_type_2 AS rel_2',
					'expired_date',
					'agent_name AS agent_name',
					'team_leader_name AS spv_name',
					'supervisor_name AS atm_name'
				);
				
				break;
		}
		
		return array($ok_file->get($fields));
	}

	private function get_recurring($product) {
		if (Auth::user()->role == 'agent') return;
		
		if (empty($product)) return;
		
		$recurring = null;
		
		switch ($product) {
			case 'mv':
				$recurring = DB::table('product_mv_polis');
				$recurring->left_join('product_mv_vehicles', 'product_mv_polis.id', '=', 'polis_id');
				
				break;
			case 'lpp':
				$recurring = DB::table('product_lpp_polis');
				$recurring->where('status', '=', 'Approved');
				$recurring->left_join(
					'product_lpp_premi',
					DB::raw("DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(IF(insured_type_1 = 'Main Insured', date_of_birth_1, date_of_birth_2))),'%Y')+0"),
					'BETWEEN',
					DB::raw('age_min and age_max and product_lpp_polis.plan_id = product_lpp_premi.plan_id')
				);
				$recurring->left_join('users_role_level', 'product_lpp_polis.user_id', '=', 'users_role_level.user_id');
				
				break;
		}
		
		if (empty($recurring)) return;
		
		if (Input::has('created_from') && Input::has('created_to')) {
			$created_from = Input::get('created_from');
			$created_to = Input::get('created_to');
			
			$recurring->where('updated', 'BETWEEN', DB::raw("'$created_from 00:00:00' AND '$created_to 23:59:59'"));
		}
		
		if (Input::has('plan_id')) {
			$recurring->where('plan_id', '=', Input::get('plan_id'));
		}
		
		if (Input::has('status')) {
			$recurring->where('status', '=', Input::get('status'));
		}
		
		$fields = array();
		
		switch($product) {
			case 'mv':
				$fields = array(
					DB::raw("NULL AS 'INSURED'"),
					DB::raw("NULL AS 'INCEPTION'"),
					DB::raw("NULL AS 'EXPIRY'"),
					DB::raw("NULL AS 'REFNO'"),
					DB::raw("NULL AS 'NO_OF_RISK'"),
					DB::raw("NULL AS 'CONJ_POLICYNO'"),
					DB::raw("NULL AS 'CONJ_CERTIFICATENO'"),
					'OBJ_INFO_01',
					'OBJ_INFO_02',
					'OBJ_INFO_03',
					'OBJ_INFO_04',
					'OBJ_INFO_05',
					'OBJ_INFO_06',
					'OBJ_INFO_07',
					'OBJ_INFO_08',
					'OBJ_INFO_09',
					'OBJ_INFO_10',
					'OBJ_INFO_11',
					'OBJ_INFO_12',
					'OBJ_INFO_13',
					'OBJ_INFO_14',
					DB::raw("NULL AS 'OBJ_INFO_15'"),
					DB::raw("NULL AS 'RISKLOCATIONREMARK'"),
					DB::raw("NULL AS 'MAINPCTADJ'"),
					DB::raw("NULL AS 'ADDINTERESTCODE1'"),
					DB::raw("NULL AS 'ADDINTERESTREMARK1'"),
					DB::raw("NULL AS 'ADDCURRENCY1'"),
					DB::raw("NULL AS 'ADDSUMINSURED1'"),
					DB::raw("NULL AS 'ADDPCTADJ1'"),
					DB::raw("NULL AS 'ADDINTERESTCODE2'"),
					DB::raw("NULL AS 'ADDINTERESTREMARK2'"),
					DB::raw("NULL AS 'ADDCURRENCY2'"),
					DB::raw("NULL AS 'ADDSUMINSURED2'"),
					DB::raw("NULL AS 'ADDPCTADJ2'"),
					DB::raw("NULL AS 'ADDINTERESTCODE3'"),
					DB::raw("NULL AS 'ADDINTERESTREMARK3'"),
					DB::raw("NULL AS 'ADDCURRENCY3'"),
					DB::raw("NULL AS 'ADDSUMINSURED3'"),
					DB::raw("NULL AS 'ADDPCTADJ3'"),
					DB::raw("NULL AS 'ADDINTERESTCODE4'"),
					DB::raw("NULL AS 'ADDINTERESTREMARK4'"),
					DB::raw("NULL AS 'ADDCURRENCY4'"),
					DB::raw("NULL AS 'ADDSUMINSURED4'"),
					DB::raw("NULL AS 'ADDPCTADJ4'"),
					DB::raw("NULL AS 'ADDINTERESTCODE5'"),
					DB::raw("NULL AS 'ADDINTERESTREMARK5'"),
					DB::raw("NULL AS 'ADDCURRENCY5'"),
					DB::raw("NULL AS 'ADDSUMINSURED5'"),
					DB::raw("NULL AS 'ADDPCTADJ5'"),
					'PRODUCT',
					DB::raw("NULL AS 'TOPRO_CURRENCY'"),
					DB::raw("NULL AS 'TOPRO_MAINSUMINSURED'"),
					DB::raw("NULL AS 'DISCOUNT'"),
					'installment AS INSTALLMENT_FREQ',
					DB::raw("NULL AS 'INSTALLMENT_PERIOD'"),
					DB::raw("NULL AS 'INSTALLMENT_FIRSTDUE'")
				);
				
				break;
			case 'lpp':
				$fields = array(
					DB::raw("NULL AS 'Nomor_Register'"),
					DB::raw("NULL AS 'Nomor_Referensi'"),
					DB::raw("CONCAT_WS(' ', SUBSTR(card_number, 1, 4), SUBSTR(card_number, 5, 4), SUBSTR(card_number, 9, 4), SUBSTR(card_number, 13, 4)) AS Nomor_Kartu_Kredit"),
					'expired_date AS Masa_Berlaku_Kredit',
					'monthly_amount AS Jumlah_Transaksi',
					DB::raw("0 AS 'Decimal_Point'"),
					'card_holder AS Nama_Pemilik_Kartu',
					DB::raw("'COMMLIFE PROT PLUS' AS 'Description'"),
					DB::raw("NULL AS 'Filler'"),
					DB::raw("NULL AS 'Alamat_Pemilik_Kartu'"),
					DB::raw("NULL AS 'Nomor_Telephone'"),
					DB::raw("NULL AS 'Periode'"),
					'agent_name AS agent_name',
					'team_leader_name AS spv_name',
					'supervisor_name AS atm_name'
				);
				
				break;
		}
		
		return array($recurring->get($fields));
	}

}
	