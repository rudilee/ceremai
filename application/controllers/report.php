<?php

class Report_Controller extends Base_Controller {

	public $restful = TRUE;

	public function __call($name, $arguments) {
		$response = array('result' => array(), 'status' => 200, 'type' => 'application/json');
		
		if (!Auth::check()) {
			$response['status'] = 401;
			$response['result'] = array('message' => 'Anda belum login');
		} else {
			$result = call_user_func_array(array($this, $name), $arguments);
			if (is_array($result)) {
				if (isset($result[0])) {
					$response['result'] = $result[0];
				}
				
				if (isset($result[1])) {
					$response['status'] = $result[1];
				}
				
				if (isset($result[2])) {
					$response['type'] = $result[2];
				}
			}
		}
		
		$response_result = $response['result'];
		
		if ($response['type'] == 'application/json') {
			$response_result = json_encode($response['result']);
		}
		
		return Response::make($response_result, $response['status'], array('Content-Type' => $response['type']));
	}
	
	private function get_excel($name, $campaign_id = 0, $called_from = 0, $called_to = 0) {
		if (Auth::user()->role == 'agent') return;
		
		include 'PHPExcel.php';

		$excel = new PHPExcel();
		$worksheet = $excel->getActiveSheet();

		$headers = array();
		$table = null;
		
		switch ($name) {
			case 'call_tracking':
				//$call_tracking = $this->get_call_tracking($campaign_id, $called_from, $called_to);
				$call_tracking = $this->get_call_tracking(Input::get('campaign_id'), Input::get('called_from'), Input::get('called_to'));
				
				$headers = $call_tracking[3];
				$table = $call_tracking[0];
				
				break;
		}
		
		$worksheet->fromArray($headers);
		
		$table_array = array();
		
		foreach($table as $table_object) {
			$table_array[] = get_object_vars($table_object);
		}
		
		$worksheet->fromArray($table_array, NULL, 'A2');
		
		$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		
		ob_start();
		$writer->save('php://output');
		$result = ob_get_contents();
		ob_end_clean();
		
		return array($result, 200, 'application/vnd.ms-excel');
	}
	
	private function get_call_tracking($campaign_id = 0, $called_from = 0, $called_to = 0) {
		//if (empty($campaign_id) || empty($called_from) || empty($called_to)) return;
		
		$fields = array(
			'agent AS Agent',
			DB::raw("CONCAT_WS(' ', first_name, last_name) AS Name"),
			"team_leader AS 'Team Leader'",
			DB::raw('COUNT(*) AS Records'),
			DB::raw('SUM(IF(called IS NOT NULL, 1, 0)) AS Utilized'),
			DB::raw('SUM(IF(call_activity.call_status_id IS NULL, 1, 0)) AS "Not Utilized"'),
			DB::raw('(SELECT COUNT(*) FROM call_tracking_attempt WHERE user_id = assignments.user_id) AS Attempt')
		);
		
		$field_names = array('Agent', 'Name', 'Team Leader', 'Records', 'Utilized', 'Not Utilize', 'Attempt');
		
		$assignments = DB::table('assignments');
		$assignments->left_join('call_activity', 'assignments.customer_id', '=', 'call_activity.customer_id');
		$assignments->left_join('call_status', 'call_activity.call_status_id', '=', 'call_status.id');
		$assignments->left_join('call_reason', 'call_activity.call_reason_id', '=', 'call_reason.id');
		$assignments->left_join('users_role_level', 'assignments.user_id', '=', 'users_role_level.user_id');
		$assignments->left_join('users', 'assignments.user_id', '=', 'users.id');
		$assignments->where_not_null('agent');
		$assignments->where('campaign_id', '=', $campaign_id);
		$assignments->group_by('assignments.user_id');
		
		foreach (DB::table('call_quality')->get() as $call_quality) {
			foreach (DB::table('call_status')->where('call_quality_id', '=', $call_quality->id)->get() as $call_status) {
				foreach (DB::table('call_reason')->where('call_status_id', '=', $call_status->id)->get() as $call_reason) {
					$fields[] = DB::raw("SUM(IF(call_activity.call_reason_id = $call_reason->id, 1, 0)) AS \"$call_reason->reason\"");
					$field_names[] = $call_reason->reason;
				}
				$fields[] = DB::raw("SUM(IF(call_activity.call_status_id = $call_status->id, 1, 0)) AS \"$call_status->status\"");
				$field_names[] = $call_status->status;
			}
		}
		
		$result = $assignments->get($fields);
		
		return array($result, 200, 'application/json', $field_names);
	}

}