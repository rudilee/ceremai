<?php

class Script_Controller extends Base_Controller {

	public $restful = TRUE;

	public function get_index() {
		return Response::json(DB::table('scripts')->get());
	}

}