<?php

class User_Controller extends Base_Controller {

	public $restful = TRUE;
	
	public function get_index() {
		$result = array();
		$status = 200;

		$user = DB::table('users');
		$user->where_in('users.id', Children::all());
		$user->where('users.id', '<>', Auth::user()->id);
		$user->left_join('users_role', 'users.id', '=', 'user_id');

		$result = $user->get(array('users.id', 'username', 'first_name', 'last_name', 'role_id'));
		
		return Response::json($result, $status);
	}
	
	public function get_detail($id = 0) {
		$result = array();
		$status = 200;

		if (!empty($id)) {
			$user = DB::table('users');
			$user->where('users.id', '=', $id);
			$user->where_in('users.id', Children::all());
			$user->left_join('users_role', 'users.id', '=', 'user_id');
			$user->left_join('users_hierarchy', 'users.id', '=', 'children_user_id');
			
			$result = $user->take(1)->get(array('users.id', 'username', 'first_name', 'last_name', 'role_id', 'parent_user_id'));
			$result = !empty($result) ? $result[0] : $result;
		}

		return Response::json($result, $status);
	}

	public function get_role() {
		$result = array();
		$status = 200;

		$roles = DB::table('role')->get(array('id', 'name'));
		$result = array();
		
		foreach ($roles as $role) {
			$result[] = array('id' => $role->id, 'name' => $role->name, 'label' => ucwords($role->name));
		}
		
		return Response::json($result, $status);
	}
	
	public function _user_by_role($role_id = 1) {
		$result = array();
		$status = 200;

		$user = DB::table('users');
		$user->where_in('users.id', Children::all());
		$user->left_join('users_role', 'users.id', '=', 'user_id');
		$user->where('role_id', '=', $role_id);

		$result = $user->get(array('users.id', 'username', 'first_name', 'last_name'))

		return Response::json($result, $status);
	}
	
	public function get_agent() {
		return Response::json($this->_user_by_role());
	}
	
	public function get_teamleader() {
		return Response::json($this->_user_by_role(2));
	}

	public function get_supervisor() {
		return Response::json($this->_user_by_role(3));
	}

	public function get_manager() {
		return Response::json($this->_user_by_role(4));
	}
	
	public function get_superior() {
		$result = array();
		$status = 200;

		$user = DB::table('users');
		$user->where_in('users.id', Children::all());
		$user->left_join('users_role', 'users.id', '=', 'user_id');
		$user->where('role_id', '>', 1);

		$result = $user->get(array('users.id', 'username', 'first_name', 'last_name'));
		
		return Response::json($result, $status);
	}
	
	public function post_new() {
		$result = array();
		$status = 200;
		$messages = array();
		
		$input = Input::all();
		
		$rules = array(
			'username' => 'required|max:10|unique:users',
			'password' => 'required|max:50',
			'password_verify' => 'required|max:50|same:password',
			'first_name' => 'required|max:40',
			'last_name' => 'max:40',
			'role_id' => 'required|in:1,2,3,4,5,6',
			'parent_user_id' => 'integer'
		);
		
		$error_messages = array(
			'password_required' => 'Field Password wajib diisi',
			'password_verify_required' => 'Field Password verifikasi wajib diisi',
			'password_verify_same' => 'Field Password verifikasi tidak sama',
			'first_name_required' => 'Field First Name wajib diisi',
			'role_id_required' => 'Field Role wajib diisi'
		);
		
		$validation = Validator::make($input, $rules, $error_messages);
		
		if ($validation->fails()) {
			$status = 400;
			
			foreach ($validation->errors->messages as $error) {
				$messages[] = $error[0];
			}
		} else {
			$values = array(
				'username' => $input['username'],
				'password' => Hash::make($input['password']),
				'first_name' => $input['first_name']
			);
			
			if (Input::has('last_name')) {
				$values['last_name'] = $input['last_name'];
			}
			
			$user_id = DB::table('users')->insert_get_id($values);
			
			DB::table('users_role')->insert(array('user_id' => $user_id, 'role_id' => $input['role_id']));
			
			if (Input::has('parent_user_id')) {
				$users_hierarchy = DB::table('users_hierarchy');
				$users_hierarchy->insert(array('parent_user_id' => $input['parent_user_id'], 'children_user_id' => $user_id));
			}
			
			$status = 201;
			$messages[] = 'User baru telah berhasil dibuat';
		}
	
		$result = array('message' => implode("\n", $messages));
		
		return json($result, $status);
	}
	
	public function put_update($id = 0) {
		$result = array();
		$status = 200;
		$messages = array();

		if (!empty($id)) {
			$input = Input::all();
			
			$rules = array(
				'password' => 'max:50',
				'password_verify' => 'max:50|same:password',
				'first_name' => 'required|max:40',
				'last_name' => 'max:40',
				'role_id' => 'required|in:1,2,3,4,5,6',
				'parent_user_id' => 'integer'
			);
			
			$error_messages = array(
				'password_verify_same' => 'Field Password verifikasi tidak sama',
				'first_name_required' => 'Field First Name wajib diisi',
				'role_id_required' => 'Field Role wajib diisi'
			);
			
			$validation = Validator::make($input, $rules, $error_messages);
			
			if ($validation->fails()) {
				$status = 400;
				
				foreach ($validation->errors->messages as $error) {
					$messages[] = $error[0];
				}
			} else {
				$values = array(
					'first_name' => $input['first_name']
				);
				
				if (Input::has('password')) {
					$values['password'] = Hash::make($input['password']);
				}
				
				if (Input::has('last_name')) {
					$values['last_name'] = $input['last_name'];
				}
				
				DB::table('users')->where('id', '=', $id)->update($values);
				
				DB::table('users_role')->where('user_id', '=', $id)->update(array('role_id' => $input['role_id']));
				
				if (Input::has('parent_user_id')) {
					if (DB::table('users_hierarchy')->where('children_user_id', '=', $id)->count() > 0) {
						$users_hierarchy = DB::table('users_hierarchy')->where('children_user_id', '=', $id);
						$users_hierarchy->update(array('parent_user_id' => $input['parent_user_id']));
					} else {
						$users_hierarchy = DB::table('users_hierarchy');
						$users_hierarchy->insert(array('parent_user_id' => $input['parent_user_id'], 'children_user_id' => $id));
					}
				}
				
				$messages[] = 'Update data user berhasil';
			}
		}

		$result = array('message' => implode("\n", $messages));
		
		return json($result, $status);
	}
	
	public function delete_delete($id = 0) {
		$result = array();
		$status = 200;

		if ($id > 1) {
			DB::table('users')->where('id', '=', $id)->delete();
			
			$result = array('message' => 'User berhasil dihapus');
		}
		
		return json($result, $status);
	}
	
	public function put_password() {
		$status = 200;
		$messages = array();
		
		$user = Auth::user();
		$input = Input::all();
		
		$rules = array(
			'old_password' => 'required|max:50',
			'password' => 'required|max:50|different:old_password',
			'password_verify' => 'required|max:50|same:password',
		);
		
		$error_messages = array(
			'ols_password_required' => 'Field Old Password wajib diisi',
			'password_required' => 'Field Password wajib diisi',
			'password_different' => 'Field Password tidak boleh sama dengan password lama',
			'password_verify_required' => 'Field Password verifikasi wajib diisi',
			'password_verify_same' => 'Field Password verifikasi tidak sama'
		);
		
		$validation = Validator::make($input, $rules, $error_messages);
		
		if ($validation->fails()) {
			$status = 400;
			
			foreach ($validation->errors->messages as $error) {
				$messages[] = $error[0];
			}
		} else {
			if (Hash::check($input['old_password'], $user->password)) {
				DB::table('users')->where('id', '=', $user->id)->update(array('password' => Hash::make($input['password'])));
				
				$messages[] = 'Password berhasil diganti';
			} else {
				$status = 400;
				$messages[] = 'Password lama anda tidak benar';
			}
		}
		
		$result = array('message' => implode("\n", $messages));
		
		return json($result, $status);
	}

}