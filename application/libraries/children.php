<?php

class Children {

	public static function all() {
		$user = Auth::user();
		$role = User::role();
		$children = array();
		$supervisor_in = array();
		$team_leader_in = array();
		
		switch ($role->name) {
			case 'administrator':
				$agents = DB::table('users');
				//$agents->left_join('users_role', 'users.id', '=', 'users_role.user_id');
				//$agents->where('role_id', '<>', '6');
				
				foreach ($agents->get(array('id')) as $agent) {
					$children[] = $agent->id;
				}
				
				break;
			case 'quality assurance':
				$agents = DB::table('users');
				$agents->left_join('users_role', 'users.id', '=', 'users_role.user_id');
				$agents->where('role_id', '=', '1');
				
				foreach ($agents->get(array('user_id')) as $agent) {
					$children[] = $agent->user_id;
				}
				
				break;
			case 'manager':
				$supervisors = DB::table('users_hierarchy')->where('parent_user_id', '=', $user->id)->where_not_null('children_user_id')
					->get('children_user_id');
				foreach ($supervisors as $supervisor) {
					$children[] = $supervisor->children_user_id;
					$supervisor_in[] = $supervisor->children_user_id;
				}
			case 'supervisor':
				if (empty($supervisor_in)) $supervisor_in[] = $user->id;
				
				$team_leaders = DB::table('users_hierarchy')->where_in('parent_user_id', $supervisor_in)->where_not_null('children_user_id')
					->get('children_user_id');
				foreach ($team_leaders as $team_leader) {
					$children[] = $team_leader->children_user_id;
					$team_leader_in[] = $team_leader->children_user_id;
				}
			case 'team leader':
				if (empty($team_leader_in)) $team_leader_in[] = $user->id;
				
				$agents = DB::table('users_hierarchy')->where_in('parent_user_id', $team_leader_in)->where_not_null('children_user_id')
					->get('children_user_id');
				foreach ($agents as $agent) {
					$children[] = $agent->children_user_id;
				}
			default:
				$children[] = $user->id;
		}
		
		return $children;
	}

}