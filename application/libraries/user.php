<?php

class User {

	public static function role() {
		if (!Session::has('user_role')) {
			$user_role_table = DB::table('users_role')->where('user_id', '=', Auth::user()->id);
			$user_role_table->left_join('role', 'users_role.role_id', '=', 'role.id');

			$user_role_result = $user_role_table->take(1)->get(array('role.id', 'name'));
			$user_role = $user_role_result[0];

			Session::put('user_role', $user_role);

			return $user_role;
		}

		return Session::get('user_role');
	}

}